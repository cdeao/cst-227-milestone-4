/* Cameron Deao
 * CST-227
 * James Shinevar
 * 6/30/2019 
 * Repo: https://bitbucket.org/cdeao/cst-227-milestone-4/src/master/ */

using Cameron_Deao_Milestone_2;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cameron_Deao_Milestone_1
{
    public partial class Grid : Form
    {
        //Creating a 2D-array for the buttons from the GameCell.
        public GameCell[,] newButtons;
        //Variables used within the class.
        public int buttonCount = 0;
        private int height = 0;
        private int width = 0;
        public static int display;

        public Grid()
        {
            InitializeComponent();
        }

        //Method to setup and display the buttons based on the user selection.
        public void SetupButtons(int totalButtons)
        {
            newButtons = new GameCell[totalButtons, totalButtons];
            //Autosizing the form.
            this.AutoSize = true;
            //Setting an int value to the total number of buttons needed.
            int overallTotal = totalButtons * totalButtons;
            //Increment counter and threshold established.
            int incrementCounter = 0;
            int incrementThreshold = 9;

            //Iterating through the 2D array to setup the buttons.
            for(int i = 0; i < newButtons.GetLength(0); i++)
            {
                for(int x = 0; x < newButtons.GetLength(1); x++)
                {
                    //If the increment counter meets the threshold
                    //the buttons spacing is changed to be on a new line.
                    if(incrementCounter == incrementThreshold)
                    {
                        incrementThreshold = incrementThreshold + 9;
                        height = 0;
                        width = width + 20;
                    }
                    GameCell newButton = new GameCell();
                    //Setting the button location.
                    newButton.Location = new Point(height + 20, width + 40);
                    height = height + 60;
                    //Adding the new button into the array.
                    newButtons[i, x] = newButton;
                    //Creating the event handler for the new button.
                    newButtons[i, x].Click += new EventHandler(ButtonClick);
                    //Setting the name based on the iteration and index.
                    newButtons[i, x].Name = "X: " + i + " " + "Y: " + x;
                    //Adding them into the controls.
                    this.Controls.Add(newButtons[i, x]);
                    //Increasing the increment counter.
                    incrementCounter++;
                }
            }
        }

        //Event handler for each button.
        private void ButtonClick(Object sender, EventArgs e)
        {
            GameCell btn = sender as GameCell;
            //Variable used to display in increment counter.
            btn.NeighborsLive++;
            //If statements that set the color based on the counter.
            if(btn.NeighborsLive == 10)
            {
                btn.BackColor = Color.Red;
            }
            if(btn.NeighborsLive == 20)
            {
                btn.BackColor = Color.Yellow;
            }
            if(btn.NeighborsLive == 30)
            {
                btn.BackColor = Color.Green;
            }
            if(btn.NeighborsLive == 40)
            {
                //Splitting the ints out of the name.
                int x = int.Parse(btn.Name.Split()[1]);
                int y = int.Parse(btn.Name.Split()[3]);
                //Showcasing the button name in a clean manner within a message box.
                MessageBox.Show("You have clicked: " + x + " " + y + " 40 times!");
            }
            //Setting the button text display.
            btn.Text = btn.NeighborsLive.ToString();
        }

        private void Grid_Load(object sender, EventArgs e)
        {
        }
    }
}
